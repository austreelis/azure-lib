{ self, ... } @ inputs:
rec {
  ###########
  # Builtins

  inherit (builtins)
    attrNames
    concatMap
    concatStringsSep
    deepSeq
    elem
    filter
    functionArgs
    getAttr
    head
    isAttrs
    isBool
    isList
    isNull
    isString
    length
    listToAttrs
    map
    match
    pathExists
    stringLength
    substring
    typeOf
  ;

  foldl = builtins.foldl';

  hasAttr = attr: set:
    substring 0 1 attr != "_" && builtins.hasAttr attr set;

  isFunction = v:
    builtins.isFunction v
    || v ? __functor && builtins.isFunction v.__functor v;

  throw = msg:
    builtins.throw (concatStringsSep "" [
      (ansi "bold" "bg-red-bright" "fg-white" null)
      "[error]"
      (ansi null)
      (" " + msg)
    ]);

  toString = v:
    if ! tryEval v then "#error#"
    else if v ? __toString then
      if isFunction v.__toString then v.__toString v
      else if isString v.__toString then v.__toString
      else throw (
        "__toString attribute is not a string, but a "
        + toPretty (typeOf v.__toString)
      )
    else if isBool v then
      if v then "true" else "false"
    else if builtins.isFunction v then "<lambda>"
    else if isNull v then "null"
    else if isAttrs v then "{ ... }"
    else if isList v then "[ ... ]"
    else builtins.toString v;

  tryEval = v: (builtins.tryEval v).success;

  ##################
  # Extra utilities

  # TODO: Use TBI azure-lib formatting pipeline
  ansi = c: rec {
    bold              =   "1";
    fg-black          =  "30";
    fg-red            =  "31";
    fg-green          =  "32";
    fg-yellow         =  "33";
    fg-blue           =  "34";
    fg-magenta        =  "35";
    fg-cyan           =  "36";
    fg-white          =  "37";
    bg-black          =  "40";
    bg-red            =  "41";
    bg-green          =  "42";
    bg-yellow         =  "43";
    bg-blue           =  "44";
    bg-magenta        =  "45";
    bg-cyan           =  "46";
    bg-white          =  "47";
    fg-black-bright   =  "90";
    fg-red-bright     =  "91";
    fg-green-bright   =  "92";
    fg-yellow-bright  =  "93";
    fg-blue-bright    =  "94";
    fg-magenta-bright =  "95";
    fg-cyan-bright    =  "96";
    fg-white-bright   =  "97";
    bg-black-bright   = "100";
    bg-red-bright     = "101";
    bg-green-bright   = "102";
    bg-yellow-bright  = "103";
    bg-blue-bright    = "104";
    bg-magenta-bright = "105";
    bg-cyan-bright    = "106";
    bg-white-bright   = "107";
    _acc = "[0";
    _eval = _acc + "m";
    __functor = self: code:
      if code == null then _eval
      else if hasAttr code self then
        throw "Invalid ansi code: '${toString code}'"
      else self // {
        _acc = _acc + ";" + self.${code};
      };
  };

  addProperties = rec {
    none = x: null;
    noneUnless = cond: f: if cond then f else addProperties.none;
    _addProperty = f: set:
      let
        oldNames = attrNames set;
        mapOldAttr = oldName:
          let
            oldValue = set.${oldName};
            newAttrs = f oldValue;
            propertyNames = attrNames newAttrs;
            pNameToAttr = p: {
              name = if oldName == "" then p else oldName + "-" + p;
              value = newAttrs.${p};
            };
          in
            if newAttrs == null then [ { name = oldName; value = oldValue; } ]
            else map pNameToAttr propertyNames;
      in listToAttrs (concatMap oldNames mapOldAttr);
    _eval = set: set;
    __functor = self: v:
      if builtins.isFunction v then
        self // { _eval = set: _addProperty v (_eval set); }
      else _eval { "" = v; };
  };

  assertMsg = cond: msg:
    if cond then x: x
    else builtins.throw "${ansi "fg-red" null}Assert failed:${ansi null} ${msg}";

  compose = f1: f2: a: f2 (f1 a);

  extendFlakeLib = flake:
    if ! flake ? lib then
      warn (
        "[extendFlakeLib] flake has no lib output, returned lib is just the "
        + "azure lib"
      )
    else if ! flake.lib ? extend then throw "Cannot extend lib"
    else flake.lib.extend (final: prev: { azure = self.lib // {
      import.module = mkImport { root = flake + "/modules"; };
    }; });

  flip = f: a: b: f b a;

  mkImport = { root ? null, inputs ? null }:
    let
      import = root: path:
        let
          assembledPath = "${root}/${path}";
          dirPath =
            if substring (stringLength assembledPath - 4) 4 == ".nix" then
              substring assembledPath 0 (stringLength assembledPath - 4)
            else assembledPath;
          filePath = "${dirPath}.nix";
          realpath =
            if pathExists "${dirPath}/default.nix" then dirPath
            else if pathExists filePath then filePath
            else throw (
              toPretty path
              + " is not a valid nix path to import from "
              + toPretty root
            );
        in builtins.import realpath;
      importFunc =
        if root == null then import
        else import root;
    in
      if inputs == null then importFunc
      else p: importFunc p inputs;

  switch = v: set: set.${toString v};

  toPretty = v:
    (
      if ! tryEval v then "${ansi "fg-red" null}##error##"
      else switch (typeOf v) {
        bool = ansi "fg-blue-bright" null + (if v then "true" else "false");
        float = ansi "fg-magenta-bright" null + (builtins.toString v);
        int = ansi "fg-magenta" null + (builtins.toString v);
        lambda =
          let
            args = functionArgs v;
            argStr =
              if args == {} then ""
              else " " + toPretty args;
          in ansi "fg-cyan" null + "<lambda${argStr}>";
        list =
          let
            str = foldl (acc: v': acc + toPretty v' + " ") "[ " v;
            color = ansi "fg-green-bright" null;
          in color + str + color + "]";
        null = ansi "fg-black-bright" null + "null";
        path = ansi "fg-yellow" null + "`${builtins.toString v}`";
        set =
          let
            str = foldl
              (acc: v': acc + v' + " = " + toPretty v.${v'} + color + "; ")
              "{ "
              (attrNames v);
            color = ansi null;
          in color + str;
        string = ansi "fg-yellow-bright" null + ''"${builtins.toString v}"'';
      }
    ) + ansi null;

  warn = msg:
    builtins.trace (concatStringsSep "" [
      (ansi "bold" "bg-yellow" "fg-white" null)
      "[warning]"
      (ansi "bold" "fg-yellow" null)
      (" " + msg)
      (ansi null)
    ]);

  warnIf = cond: msg: if cond then warn msg else x: x;

  ############
  # Constants
  
  hostnameRegex = "[a-z][a-z0-9-]{2,61}[a-z0-9]";
}
