{
  description = "A standalone general-purpose nix lib";

  outputs = { ... } @ inputs: { lib = import ./. inputs; };
}
